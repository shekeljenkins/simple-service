# lior add
# Simple Service

This is a simple web Service

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This project need Java Run Time Env 8 and mysql db 5.X (tested with 5.7.23) in order to run

### Installing

To run the server you just need to run the jar as executable
```
./demo-0.0.1-SNAPSHOT.jar
```


### Running

the service will connected to the configured db and create 10 messages random records, each message consists of `id` and `payload`

the web service will listen to port 8080 on localhost and expose GET and POST urls, with the following api

`GET localhost:8080/api/v1/message` - will retrive all db records in a JSON format

`POST localhost:8080/api/v1/message` with `{ "payload": "some string" }` (JSON object as request body) - will insert a record to the db


the server will run with the default configurations

```
server.port=8080
spring.datasource.url: jdbc:mysql://localhost:3306/shekeldb?createDatabaseIfNotExist=true
spring.datasource.username: root
spring.datasource.password: 1234
```

To override the default configuration create a file name `application.properties` with the above configurations and place it at the same folder as the executable jar file



